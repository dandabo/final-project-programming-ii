# Final Project Programming II

**Group Members:**

Aaleyah Gibson
Diayana Andabo

**Project:**

This project is the snake game but with a twist, we created it into a GUI and ran through a CLI that included GPIO buttons programmed to be the arrow keys.
The language we used for all the programming was python and we programmed this game on our Raspberry Pi. We also customized the snake and 
its food to our preference. We made the snake Doja Cat and her food a burger. When you lose the game it also shows you where you colided 
by giving you the exact coordinates of the collision. 

We hope you enjoy our game! :)